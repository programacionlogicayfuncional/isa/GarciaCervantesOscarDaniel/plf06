(ns plf06.core
  (:gen-class))
;Alfabeto
(def minu "aábcdeéfghiíjklmnñoópqrstuúüvwxyz")

(def may "AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ")

(def carac "01234!´#$%&'()*+,-./:;<=>?@[ø]^_`{|}~56789")

;Version recursiva del rot13
(defn encrip
  [s]
  (letfn [(f [c]
            (if (empty? c)
              ""
                  (if (clojure.string/includes? may (str (first c)))
                    (str (first (drop (mod (+ 13 (.indexOf may (str (first c)))) 33) may)) (f (rest c)))
                    (if (clojure.string/includes? minu (str (first c)))
                      (str (first (drop (mod (+ 13 (.indexOf minu (str (first c)))) 33) minu)) (f (rest c)))
                      (if (clojure.string/includes? carac (str (first c))) 
                        (str (first (drop (mod (+ 13 (.indexOf carac (str (first c)))) 42) carac)) (f (rest c))) 
                        (str (first c) (f (rest c))))))))]
    (f s)))

;Acepta varios argumentos
(defn -main
  [& args]
  (if (empty? args)
  (println "Error. No enviaste ningun argumento")
  (println (encrip (apply str args)))))